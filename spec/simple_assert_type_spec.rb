require 'spec_helper'

describe SimpleAssertType do

  describe 'assert_type' do
    context 'passing a correct parameter' do
      it 'should not raise an ArgumentError' do
        expect { assert_type(1, Fixnum) }.to_not raise_error
      end
    end
    context 'passing a String' do
      context 'with a Fixnum type' do
        it 'should raise an ArgumentError with message "Fixnum expected, got String"' do
          expect { assert_type("1", Fixnum) }.to raise_error('Fixnum expected, got String')
        end
      end
      context 'with an Array type' do
        it 'should raise an ArgumentError with message "Array expected, got String"' do
          expect { assert_type("1", Array) }.to raise_error('Array expected, got String')
        end
      end
      context 'with a String type' do
        it 'should not raise an ArgumentError' do
          expect { assert_type("1", String) }.to_not raise_error
        end
      end
    end
  end

end
