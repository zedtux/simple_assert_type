# SimpleAssertType

[![Build Status](https://travis-ci.org/zedtux/simple_assert_type.png?branch=master)](https://travis-ci.org/zedtux/simple_assert_type)

Very simple assert_type method that you can use anywhere in order to check
a variable type.

In case the type is not the same, an `ArgumentError` is raised.

## Installation

Add this line to your application's Gemfile:

    gem 'simple_assert_type'

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install simple_assert_type

## Usage


    ree-1.8.7-2012.02 :001 > assert_type 1, Fixnum
    => true
    ree-1.8.7-2012.02 :002 > assert_type 1, String
    ArgumentError: String expected, got Fixnum
      from /Users/guillaumeh/Developments/simple_assert_type/lib/simple_assert_type.rb:8:in `assert_type'
      from (irb):2


## Contributing

1. Fork it ( http://github.com/<my-github-username>/simple_assert_type/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request
