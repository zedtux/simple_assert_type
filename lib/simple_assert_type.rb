require "simple_assert_type/version"

module SimpleAssertType
  def self.extended(mod)
    mod.module_eval do
      def assert_type(value, type)
        return true if value.is_a?(type)
        raise ArgumentError, "#{type} expected, got #{value.class}"
      end
    end
  end
end

# Extend the Ruby Kernel module
module Kernel
  extend SimpleAssertType
end
