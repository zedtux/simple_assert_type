# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'simple_assert_type/version'

Gem::Specification.new do |spec|
  spec.name          = "simple_assert_type"
  spec.version       = SimpleAssertType::VERSION
  spec.authors       = ["zedtux"]
  spec.email         = ["zedtux@zedroot.org"]
  spec.summary       = %q{Simple assert_type method for Ruby.}
  spec.description   = %q{Simple assert_type method in order to ensure that given parameters are of the right type.}
  spec.homepage      = "https://github.com/zedtux/simple_assert_type"
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.5"
  spec.add_development_dependency "rake"
  spec.add_development_dependency "rspec"
  spec.add_development_dependency "simplecov"
end
